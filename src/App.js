import {React, useState} from 'react';
import './App.css';

export default function App(){
    const [counter, setCounter] = useState(0);
    const [savedCounter, setSavedCounter] = useState([]);

    const increase = () => {
        setCounter(preCounter => preCounter + 1);
    }
    
    const decrease = () => {
        if (counter > 0){
            setCounter(preCounter => preCounter - 1)            
        }
    }

    const reset = () => {
        setCounter(0)

    }

    const saveHistory = () => {
        setSavedCounter([...savedCounter, counter])
        setCounter(0)
    }

    const historyDisplay = savedCounter.map( history => {
        return <li>{history} laps</li>
    })

    return(
        <div className="counter">
            <h1>Lap Counter</h1>
            <span className="counter-output">{counter} laps</span>
            <div className="button-container">
                <button className="button-control" onClick={increase}>+</button>
                <button className="button-control" onClick={decrease}>-</button>
                <button className="reset" onClick={reset}>Reset</button>
                <button className="save" onClick={saveHistory}>Save</button>
            </div>
            <hr />
            <h2>Previous Records</h2>
            <ul className="records">
                {historyDisplay}
            </ul>
        </div>
    )
}