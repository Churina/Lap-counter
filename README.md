                                                      **Lap counter exercise v1**

Catalyte is working with a health/fitness client to build an application to track laps for joggers. Users need a way to add laps as they are completed and remove them if they accidentally add one. Users also need to be able to reset the number of laps so they
have a fresh start for each run session.

- Requirements:

1. Application must display the current number of laps for the user.
2. A button to increment the lap count.
3. A button to decrement the lap count. Should only decrement if the lap count is greater than zero.
4. A button to reset the total number of laps.
5. Component state should be updated immutably.

- Stretch Requirements (These are optional requirementsfor additional practice):

1. Create a way to "save" a lap count which displays the number of laps for a
session in a list below the counter. Saving a session should reset the lap count.
